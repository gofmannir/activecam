#!/usr/bin/env python

from __future__ import division
import cv2
import numpy as np
import socket
import struct
import math
import imutils
import sys, zlib
import gzip, lzma
from io import BytesIO

class FrameSegment(object):
    """
    Object to break down image frame segment
    if the size of image exceed maximum datagram size
    """
    MAX_DGRAM = 2**16
    MAX_IMAGE_DGRAM = MAX_DGRAM - 64 # extract 64 bytes in case UDP frame overflown
    def __init__(self, sock, port, addr="127.0.0.1"):
        self.s = sock
        self.port = port
        self.addr = addr

    def udp_frame(self, img):
        """
        Compress image and Break down
        into data segments
        """
        """
         IMWRITE_JPEG_QUALITY        = 1,  //!< For JPEG, it can be a quality from 0 to 100 (the higher is the better). Default value is 95.
       IMWRITE_JPEG_PROGRESSIVE    = 2,  //!< Enable JPEG features, 0 or 1, default is False.
       IMWRITE_JPEG_OPTIMIZE       = 3,  //!< Enable JPEG features, 0 or 1, default is False.
       IMWRITE_JPEG_RST_INTERVAL   = 4,  //!< JPEG restart interval, 0 - 65535, default is 0 - no restart.
       IMWRITE_JPEG_LUMA_QUALITY   = 5,  //!< Separate luma quality level, 0 - 100, default is 0 - don't use.
       IMWRITE_JPEG_CHROMA_QUALITY = 6,  //!< Separate chroma quality level, 0 - 100, default is 0 - don't use.
       IMWRITE_PNG_COMPRESSION     = 16, //!< For PNG, it can be the compression level from 0 to 9. A higher value means a smaller size and longer compression time. Default value is 3.
       IMWRITE_PNG_STRATEGY        = 17, //!< One of cv::ImwritePNGFlags, default is IMWRITE_PNG_STRATEGY_DEFAULT.
       IMWRITE_PNG_BILEVEL         = 18, //!< Binary level PNG, 0 or 1, default is 0.
       IMWRITE_PXM_BINARY          = 32, //!< For PPM, PGM, or PBM, it can be a binary format flag, 0 or 1. Default value is 1.
       IMWRITE_WEBP_QUALITY        = 64  //!< For WEBP, it can be a quality from 1 to 100 (the higher is the better). By default (without any parameter) and for quality above 100 the lossless compression is used.
     };
        """
        encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 20,
                        int(cv2.IMWRITE_JPEG_PROGRESSIVE), 1,
                        int(cv2.IMWRITE_JPEG_OPTIMIZE), 1,
                        ]
        compress_img = cv2.imencode('.jpg', img, encode_param)[1]
        dat = compress_img.tostring()
        size = len(dat)
        count = math.ceil(size/(self.MAX_IMAGE_DGRAM))
        array_pos_start = 0
        while count:
            array_pos_end = min(size, array_pos_start + self.MAX_IMAGE_DGRAM)
            self.s.sendto(struct.pack("B", count) +
                dat[array_pos_start:array_pos_end],
                (self.addr, self.port)
                )
            array_pos_start = array_pos_end
            count -= 1

def getsizestr(stro):
    return len(str(stro).encode('utf-8')) / (1000 * 1000)

def main():
    """ Top level main function """
    # Set up UDP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    port = 12345

    fs = FrameSegment(s, port)

    cap = cv2.VideoCapture(0)
    while (cap.isOpened()):
        _, frame = cap.read() # size is 900
        frame = imutils.resize(frame, width=320) # size 445
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # size 148
        #frame = np.dstack([frame, frame, frame])

        #mode = frame.mode  # RGB, RGBA, L
        #format_ = frame.format  # JPEG, PNG, JPG etc
        #w, h = frame.size
        #print(type(frame))
        #print(frame.dtype)
        #print(frame.size)
        # frame_bytes = frame.tobytes()
        # buffer = BytesIO(frame_bytes)
        # size_kb = buffer.getbuffer().nbytes / 1024  # Original Image size in kb
        # print("Size: " + str(size_kb))
        #
        # # new_frame = np.frombuffer(buffer.getbuffer(), dtype=np.uint8)
        # # print("new frame: ")
        # # print(type(new_frame))
        # # print(new_frame.dtype)
        # # print(new_frame.size)
        # encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 30]
        # img_str = cv2.imencode('.jpg', frame, encode_param)[1].tostring()
        # print(f"image size before: {getsizestr(img_str) :,} MB")
        #
        # nparr = np.fromstring(img_str, np.uint8)
        # new_frame2 = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        fs.udp_frame(frame)

    cap.release()
    cv2.destroyAllWindows()
    s.close()

if __name__ == "__main__":
    main()